import Vue from 'vue'
import VueRouter from 'vue-router'
import Homepage from '../components/Homepage.vue'
import Cart from '../components/Cart.vue'
import Products from '../components/Products.vue'
import ProductSheet from '../components/ProductSheet.vue'
import UserCommands from '../components/UserCommands.vue'
import Login from '../components/Login.vue'
import SignUp from '../components/SignUp.vue'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'homepage',
    component: Homepage
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },

  
  {
    path: '/cart',
    name: 'cart',
    component: Cart
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    // component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/products',
    name: 'products',
    component: Products,
    
  },
  {
    path: '/product-sheet/:productId',
    name: 'productSheet',
    component: ProductSheet,
  },
  {
    path: '/user-commands',
    name: 'userCommands',
    component: UserCommands,
  },
  {
    path: '/sign-up',
    name: 'signUp',
    component: SignUp,
  },
]

const router = new VueRouter({
  routes
})

export default router
