import Vue from 'vue'
import Vuex from 'vuex'


Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    products: [],
    cartProducts:[],
  },

  mutations: {
    SET_PRODUCTS(state, products) { 
      state.products = products;
    },
    SET_CARTPRODUCTS(state, cartProducts) {
      state.cartProducts = cartProducts;
    },
    DELETE_ARTICLE(state, index) {
      state.cartProducts.splice(index, 1);
    },
  },

  getters: {
  },

  actions: {
    getProductsToStore() {
      fetch('../../data/db_products.json') 
      .then(function(response) {
        return response.json();
      })
      .then(response => {
         this.commit('SET_PRODUCTS', response.products);
      })   
    },
    setCartProductsToStore() {
      fetch('../../data/db_cart_products.json') 
      .then(function(response) {
        return response.json();
      })
      .then(response => {
        console.log(response);
         this.commit('SET_CARTPRODUCTS', response.cartProducts);
      })   
    },
    
    AUTHENTICATE({state}, activate) {
      state.authentification = activate;
    }
  },
  modules: {}
})